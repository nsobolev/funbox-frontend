import React from 'react';

import ProductItem from '../ProductItem';

const ProductList = ({ state, toggleSelectedProduct, setLeaveSelectedProduct }) => {
  return (
    <ul className="product-list">
      {
        state.products.map(product => {
          const selectedProduct = state.selectedProducts.length > 0 &&
            state.selectedProducts.find(selectedProduct => selectedProduct.id === product.id);

          const isLeave = selectedProduct ? selectedProduct.isLeave : false;

          return <li className="product-list__item" key={ product.id }>
            <ProductItem
              { ...product }
              toggleSelectedProduct={ toggleSelectedProduct }
              setLeaveSelectedProduct={ setLeaveSelectedProduct }
              isLeave={ isLeave }
              isSelected={ Boolean(selectedProduct) }
            />
          </li>
        })
      }
    </ul>
  )
};

export default ProductList;
