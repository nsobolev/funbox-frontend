import React from 'react';

import ProductCaption from '../ProductCaption';

import { declOfNum } from '../../../utils/declOfNum';
import classNames from 'classnames';

const ProductItem = (
  {
    id,
    description,
    name,
    flavor,
    src,
    weight,
    portionCount,
    giftCount,
    customerReview,
    caption,
    stockCount,
    className,
    toggleSelectedProduct,
    setLeaveSelectedProduct,
    isSelected,
    isLeave,
  }) => {
  const isDisabled = stockCount === 0;
  const isSelectedIndicator = isSelected && isLeave;

  return (
    <div
      className={
        classNames(
          'product-item',
          { [className]: Boolean(className) },
          { 'product-item_disabled': isDisabled },
          { 'product-item_select': isSelectedIndicator })
      }>
      <div className="product-item__card"
           onClick={ () => !isDisabled ? toggleSelectedProduct({ id, isLeave: false }) : null }
           onMouseLeave={() => !isDisabled && isSelected && !isLeave ? setLeaveSelectedProduct(id) : null }
      >
        <div className="product-item__content">
          <p className="product-item__description">{ description }</p>
          <h3 className="product-item__slogan">{ name }</h3>
          <p className="product-item__flavor">{ flavor }</p>
          <div className="product-item__characteristic">
            <p className="product-item__parameter">
              <span className="product-item__parameter-count">{ portionCount }</span> { declOfNum(portionCount, ['порция', 'порции', 'порций']) }
            </p>
            <p className="product-item__parameter">
              { giftCount > 1 && <span className="product-item__parameter-count">{ giftCount }</span> } { declOfNum(giftCount, ['мышь', 'мыши', 'мышей']) } в подарок
            </p>
            { customerReview && <p className="product-item__parameter">{ customerReview }</p> }
          </div>
        </div>
        <div className="product-item__image-container">
          <img className="product-item__image" src={ src } alt={ name } />
          <div className="product-item__weight">
            <span className="product-item__weight-count">{ weight }</span> кг
          </div>
        </div>
      </div>
      <ProductCaption
        id={ id }
        caption={ isDisabled ? flavor : caption }
        toggleSelectedProduct={ toggleSelectedProduct }
        isDisabled={ isDisabled }
        isSelected={ isSelected }
      />
    </div>
  );
};

ProductItem.defaultProps = {
  toggleSelectedProduct: () => {},
  setLeaveSelectedProduct: () => {},
  customerReview: '',
  className: '',
  isDisabled: false,
};

export default ProductItem;
