import React, { useReducer, useCallback } from 'react';

import reducer from './state/reducer';
import initialState from './state/initialState';
import actions from './state/actions';

import ProductList from './ProductList';

const ProductPage = () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const toggleSelectedProduct = useCallback((payload) => {
    if (state.selectedProducts.some(selectedProduct => selectedProduct.id === payload.id)) {
      dispatch(actions.removeSelectedProduct(payload.id));
    } else {
      dispatch(actions.addSelectedProduct(payload));
    }
  }, [state.selectedProducts]);

  const setLeaveSelectedProduct = useCallback((id) => {
    dispatch(actions.setLeaveSelectedProduct(id));
  }, [state.selectedProducts]);

  return (
    <section className="product-page">
      <div className="product-page__container container">
        <h2 className="product-page__slogan">Ты сегодня покормил кота?</h2>
        <div className="product-page__products">
          <ProductList
            state={ state }
            toggleSelectedProduct={ toggleSelectedProduct }
            setLeaveSelectedProduct={ setLeaveSelectedProduct }
          />
        </div>
      </div>
    </section>
  )
};

export default ProductPage;
