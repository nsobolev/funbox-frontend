import React from 'react';

import classNames from 'classnames';

const ProductCaption = ({ id, caption, toggleSelectedProduct, isSelected, isDisabled }) => {
  let content = <>Чего сидишь? Порадуй котэ, <button className="product-caption__button" onClick={ () => toggleSelectedProduct({ id, isLeave: true }) }>купи.</button></>;

  if (isDisabled) {
    content = `Печалька, ${caption} закончился.`;
  } else if (isSelected) {
    content = caption;
  }

  return (
    <div className={ classNames('product-caption', { 'product-caption_disabled': isDisabled }) }>{ content }</div>
  )
};

ProductCaption.defaultProps = {
  caption: '',
  isSelected: false,
  isDisabled: false,
};

export default ProductCaption;
