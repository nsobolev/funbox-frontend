import types from './types';

export default {
  addSelectedProduct(id) {
    return { type: types.addSelectedProduct, payload: id };
  },
  setLeaveSelectedProduct(id) {
    return { type: types.setLeaveSelectedProduct, payload: id };
  },
  removeSelectedProduct(id) {
    return { type: types.removeSelectedProduct, payload: id };
  }
}
