const prefix = 'PRODUCTS:';

export default {
  addSelectedProduct: `${prefix}ADD_SELECTED_PRODUCT`,
  setLeaveSelectedProduct: `${prefix}SET_LEAVE_SELECTED_PRODUCT`,
  removeSelectedProduct: `${prefix}REMOVE_SELECTED_PRODUCT`,
}
