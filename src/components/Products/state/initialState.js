import products from '../utils/products';

export default {
  products,
  selectedProducts: [],
};
