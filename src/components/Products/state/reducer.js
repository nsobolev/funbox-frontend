import types from './types';

export default (state, action) => {
  switch (action.type) {
    case types.addSelectedProduct:
      return {
        ...state,
        selectedProducts: [...state.selectedProducts, action.payload],
      };

    case types.setLeaveSelectedProduct: {
      return {
        ...state,
        selectedProducts: state.selectedProducts.map(selectedProduct => selectedProduct.id === action.payload ?
          { ...selectedProduct, isLeave: true } : selectedProduct)
      }
    }

    case types.removeSelectedProduct:
      return {
        ...state,
        selectedProducts: state.selectedProducts.filter(selectedProduct => selectedProduct.id !== action.payload),
      };

    default:
      return state;
  }
};
