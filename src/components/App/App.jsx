import React from 'react';

import '../../styles/bootstrap.less';

import ProductPage from '../Products'

const App = () => {
  return (
    <div className="wrapper">
      <ProductPage />
    </div>
  )
};

export default App;
